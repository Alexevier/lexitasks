use std::sync::mpsc;
use std::sync::mpsc::TryRecvError;
use std::time::Duration;
use crate::config;
use crate::Task;
use crate::List;
use std::sync::{Mutex, Arc};
use crate::ThreadMessage;

//function modules
mod load;
mod save;

pub struct LexiTasks {
	exit: bool,
	lists: Arc<Mutex<Vec<List>>>,
	autosaveTimer: lexlib::Timer,
	tx: mpsc::Sender<ThreadMessage>,
	rx: mpsc::Receiver<ThreadMessage>,
}

impl LexiTasks {
pub const VERSION: u16 = 1;
	
pub fn update(&mut self){
	match self.rx.try_recv() {
		Ok(msg) => match msg {
			ThreadMessage::terminate => {self.terminate(); return ();},
			ThreadMessage::error(v) => println!("{}",v),
			ThreadMessage::newTask(id) => {
				let id = self.newTask(id);
				self.tx.send(ThreadMessage::createdTask(id)).expect("did Ui thread die?");
			}
			ThreadMessage::newList => {self.newList();},
			_ => println!("LexiTasks received unregistered threadMessage")
		}
		Err(e) => match e {
			TryRecvError::Empty => {},
			TryRecvError::Disconnected => {
				panic!("did LexiUi thread die?")
			}
		}
	};
	
	//auto save
	self.autosaveTimer.update();
	if self.autosaveTimer.done() {
		self.autosaveTimer.set(Duration::from_secs(config().autosaveTime.into()));
		self.autosaveTimer.start();
		match self.save() {
			Ok(..) => {},
			Err(e) => self.tx.send(ThreadMessage::error(e.into())).expect("did Ui thread die?")
		}
	}
	
	
	std::thread::sleep(Duration::from_millis(config().updateTime as u64));
}

pub fn new(rx: mpsc::Receiver<ThreadMessage>, tx: mpsc::Sender<ThreadMessage>,) -> Self {
	Self{
		exit: false,
		lists: Arc::new(Mutex::new(Vec::new())),
		autosaveTimer: lexlib::Timer::new(Duration::from_secs(config().autosaveTime.into())),
		tx,
		rx,
	}
}

/// Creates a new task and returns his id.
pub fn newTask(&mut self, listId: u32) -> (u32,u32) {
	let task = Task::default();
	let id = task.id;
	for list in self.lists.lock().unwrap().iter_mut() {
		if list.id() == listId {
			if config().createTasksTop {
				list.tasks.insert(0,task);
			} else {
				list.tasks.push(task);
			}
			break;
		}
	}
	(listId,id)
}

fn newList(&mut self) -> u32 {
	let list = List::new();
	let id = list.id();
	self.lists.lock().unwrap().push(list);
	id
}

pub fn getLists(&self) -> Arc<Mutex<Vec<List>>> {
	self.lists.clone()
}

pub fn exit(&self) -> bool {
	self.exit
}

fn terminate(&mut self) {
	self.exit = true;
	let mut correctlyTerminated = true;
	match config().save() {
		Ok(..) => {}
		Err(e) => {
			println!("{}",e);
			correctlyTerminated = false;
		}
	}
	match self.save() {
		Ok(..) => {},
		Err(e) => {
			println!("{}",e);
			correctlyTerminated = false;
		}
	}
	if !correctlyTerminated {
		println!("terminated with errors")
	}
}
}