pub struct List {
	pub name: String,
	id: u32,
	pub tasks: Vec<crate::Task>,
}

impl List {
pub fn new() -> Self {
	static mut LTAKENIDS: u32 = 0;
	unsafe { LTAKENIDS += 1 };
	Self{
		name: "NULL".into(),
		id: unsafe{ LTAKENIDS },
		tasks: Vec::new(),
	}
}
pub fn id(&self) -> u32 {
	self.id
}
}