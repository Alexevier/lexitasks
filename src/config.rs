// here is no need for a mutex
static mut CONFIG: Config = Config::defaultConst();

use std::fs::File;
use std::fs::remove_file;
use std::fs::create_dir;
use std::io::Write;
use std::io::Read;
use core::fmt;
use lexlib::color;
use std::io::ErrorKind;


pub fn config() -> &'static mut Config {
	unsafe { &mut CONFIG}
}

/* NOTE s
 * glow renderer requires to have hardwareAcceleration set to true otherwise panics with the message "failed to find a matching configuration for creating opengl context".
 * wgpu renderer doesn't require to have hardwareAcceleration.
*/

pub struct Config{
	/// will show icons instead of text as buttons
	pub showIcons: bool,
	/// shows tasks that are done in the same panel of the ones that are not done, also hides the done panel.
	pub showDoneSamePanel: bool,
	/// shows the panel of tasks done
	pub showDonePanel: bool,
	/// always shows scrollbars
	pub showScrollAlways: bool,
	/// hides the dangerous configs that could break something.
	pub showDangerousConfigs: bool,
	/// puts the sidepanel on the left if true, on the right otherwise
	pub sidePanelLeft: bool,
	/// hides the notes section of a task if empty
	pub hideEmptyNotes: bool,
	/// creates new tasks at the top of the list if true.
	pub createTasksTop: bool,
	pub colorUrgent: color::RGB,
	pub colorHigh: color::RGB,
	pub colorNormal: color::RGB,
	pub colorLow: color::RGB,
	pub colorNone: color::RGB,
	/// enables hardware acceleration, doesn't guaranties it will be used but allow to use it
	pub hardwareAcceleration: bool,
	/// the eframe renderer to be used
	pub renderer: eframe::Renderer,
	/// shader version for glow
	pub shaderVersion: eframe::egui_glow::ShaderVersion,
	/// save data every so many seconds set, might not be accuarate but does the job
	pub autosaveTime: u16,
	/// updates every so many second to respond to Ui.
	pub updateTime: u16,
}

impl Config {
const fn defaultConst() -> Self {
	Self {
		showIcons: true,
		showDoneSamePanel: false,
		showDonePanel: true,
		showScrollAlways: false,
		showDangerousConfigs: false,
		sidePanelLeft: true,
		hideEmptyNotes: false,
		createTasksTop: true,
		colorUrgent: color::RGB::from_rgb(0x8b,0x00,0x00),
		colorHigh: color::RGB::from_rgb(0x9d,0x71,0x1f),
		colorNormal: color::RGB::BLACK,
		colorLow: color::RGB::BLACK,
		colorNone: color::RGB::BLACK,
		hardwareAcceleration: false,
		renderer: eframe::Renderer::Glow,
		shaderVersion: eframe::egui_glow::ShaderVersion::Gl140,
		autosaveTime: 45,
		updateTime: 50,
	}
}

pub fn load(&mut self) {
	let mut configPath = dirs::config_dir().unwrap();	
	configPath.push(env!("CARGO_PKG_NAME"));
	configPath.push("config");
	
	//open file or create if doesn't exist
	let mut file = match File::open(configPath) {
		Ok(f) => f,
		Err(..) => {
			match self.save() {
				Ok(..) => return (),
				Err(..) => {println!("can't create/find config file. using defaults"); return ()}
			}
		}
	};
	
	let mut content = String::new();
	match file.read_to_string(&mut content) {
		Ok(..) => {},
		Err(..) => {println!("can't read/invalid config file. using defaults"); return();}
	}
	
	let mut content = content.lines();
	content.next();
	'parsing: loop {
		let line = match content.next() {
			Some(d) => d,
			None => break 'parsing
		};
		let data: Vec<&str> = line.split(':').collect();
		if data.len() != 2 || data[1].is_empty() {
			println!("{}", ["missing config at: ", data[0]].concat());
			continue 'parsing
		}
		
		match data[0] {
			"showIcons" => self.showIcons = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"showDonePanel" => self.showDonePanel = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"showDoneSamePanel" => self.showDoneSamePanel = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"showScrollAlways" => self.showScrollAlways = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"showDangerousConfigs" => self.showDangerousConfigs = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"sidePanelLeft" => self.sidePanelLeft = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"hideEmptyNotes" => self.hideEmptyNotes = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"createTasksTop" => self.createTasksTop = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"colorUrgent" => self.colorUrgent = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"colorHigh" => self.colorHigh= match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"colorNormal" => self.colorNormal = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"colorLow" => self.colorLow= match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"colorNone" => self.colorNone= match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"hardwareAcceleration" => self.hardwareAcceleration = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"renderer" => self.renderer= match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"shaderVersion" => self.shaderVersion = match data[1] {
				"gl120" => eframe::egui_glow::ShaderVersion::Gl120,
				"gl140" => eframe::egui_glow::ShaderVersion::Gl140,
				"es100" => eframe::egui_glow::ShaderVersion::Es100,
				"es300" => eframe::egui_glow::ShaderVersion::Es300,
				_ => {println!("{}", ["invalid string", " at config: ", data[0]].concat()); continue 'parsing}
			},
			"autosaveTime" => self.autosaveTime = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			"updateTime" => self.updateTime = match data[1].parse() {
				Ok(v) => v,
				Err(e) => {println!("{}", [&e.to_string(), " at config: ", data[0]].concat()); continue 'parsing}
			},
			_ => println!("{}", ["unkown config: ", data[0]].concat())
		}
	}
	
	//glow seems to require hardwareAcceleration
	if self.renderer == eframe::Renderer::Glow && !self.hardwareAcceleration {
		self.hardwareAcceleration = true;
		println!("hardwareAcceleration set to true because of eframe::Renderer::Glow");
	}
	println!("config loaded");
}

pub fn save(&self) -> Result<(), &str> { // TODO error handeling requires cleanup
	let mut configPath = match dirs::config_dir() {
		Some(p) => p,
		None => return Err("couldn't find config_dir")
	};
	configPath.push(env!("CARGO_PKG_NAME"));
	configPath.push("config");
	
	match File::create(configPath.clone()) {
		Ok(mut file) => {
			match file.write(config().to_string().as_bytes()) {
				Ok(..) => println!("config saved"),
				Err(..) => { println!("can't save config"); return Err("can't save config"); }
			};
			return Ok(());
		},
		Err(e) => match e.kind() {
			ErrorKind::AlreadyExists=> {
				match remove_file(configPath.parent().unwrap()) {
					Ok(..) => return self.save(), //recursion :)
					Err(e) => match e.kind() {
						ErrorKind::NotFound => {
							match create_dir(configPath.parent().unwrap()) {
								Ok(..) => return self.save(),
								Err(e) => match e.kind() {
									ErrorKind::PermissionDenied => return Err("Permision denied for creating lexitasks folder in configs folder"),
									_ => return Err("unkown/unregisterd error ocurred when creating config dir\nafter delete and create")
								}
							}
						},
						ErrorKind::PermissionDenied => return Err("Permision denied for config file"),
						_ => return Err("unkown/unregisterd error ocurred when creating config dir\nafter delete")
					}
				}
			},
			ErrorKind::NotFound => {
				match create_dir(configPath.parent().unwrap()) {
					Ok(..) => return self.save(),
					Err(e) => match e.kind() {
						ErrorKind::PermissionDenied => return Err("Permision denied for creating lexitasks folder in configs folder"),
						ErrorKind::AlreadyExists => match remove_file(configPath.parent().unwrap()) {
							Ok(..) => return self.save(),
							Err(..) => return Err("unkown/unregisterd error ocurred when creating config dir\nafter create")
						},
						_ => return Err("unkown/unregisterd error ocurred when creating config dir\nafter create")
					}
				}
			},
			ErrorKind::PermissionDenied => return Err("Permision denied for creating config file"),
			_ => return Err("unkown/unregisterd error ocurred when creating config file\nend")
		}
	};
}
}

impl fmt::Display for Config{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(f,
			"this file shouldn't be manually touched\n
showIcons:{}
showDonePanel:{}
showDoneSamePanel:{}
showScrollAlways:{}
showDangerousConfigs:{}
sidePanelLeft:{}
hideEmptyNotes:{}
createTasksTop:{}
colorUrgent:{}
colorHigh:{}
colorNormal:{}
colorLow:{}
colorNone:{}
hardwareAcceleration:{}
renderer:{}
shaderVersion:{}
autosaveTime:{}
updateTime:{}",
			&self.showIcons.to_string(),
			&self.showDonePanel.to_string(),
			&self.showDoneSamePanel.to_string(),
			&self.showScrollAlways.to_string(),
			&self.showDangerousConfigs.to_string(),
			&self.sidePanelLeft.to_string(),
			&self.hideEmptyNotes.to_string(),
			&self.createTasksTop.to_string(),
			&self.colorUrgent.to_string(),
			&self.colorHigh.to_string(),
			&self.colorNormal.to_string(),
			&self.colorLow.to_string(),
			&self.colorNone.to_string(),
			&self.hardwareAcceleration.to_string(),
			&self.renderer.to_string(),
			match &self.shaderVersion {
				eframe::egui_glow::ShaderVersion::Gl120 => "gl120",
				eframe::egui_glow::ShaderVersion::Gl140 => "gl140",
				eframe::egui_glow::ShaderVersion::Es100 => "es100",
				eframe::egui_glow::ShaderVersion::Es300 => "es300",
			},
			&self.autosaveTime.to_string(),
			&self.updateTime.to_string(),
		)
	}
}