// Acceptable Heresy :)
#![allow(non_snake_case)]
#![allow(non_camel_case_types)]
#![allow(unused_parens)]

mod lexiUi;
mod lexiTasks;
mod taskEditor;
mod listEditor;
mod config;
mod task;
mod list;
mod icon;

use lexiUi::*;
use lexiTasks::*;
use taskEditor::TaskEditor;
use listEditor::ListEditor;
use config::*;
use task::*;
use list::*;
use lexlib::Vec2;
use std::thread;
use std::sync::mpsc;

fn main() {
	config().load();
	let eOptions = eframe::NativeOptions {
		always_on_top: false,
		maximized: false,
		decorated: true,
		fullscreen: false,
		drag_and_drop_support: false,
		icon_data: Some(icon::icon()),
		initial_window_pos: None,
		initial_window_size: Some(Vec2::new(1024,600).into()),
		min_window_size: Some(Vec2::new(640,480).into()),
		max_window_size: None,
		resizable: true,
		transparent: true,
		mouse_passthrough: false,
		vsync: true,
		multisampling: 0,
		depth_buffer: 0,
		stencil_buffer: 0,
		hardware_acceleration: if !config().hardwareAcceleration {
				eframe::HardwareAcceleration::Off
			} else {
				eframe::HardwareAcceleration::Preferred
			},
		renderer: config().renderer,
		follow_system_theme: false,
		default_theme: eframe::Theme::Dark,
		run_and_return: true,
		event_loop_builder: None,
		shader_version: Some(config().shaderVersion),
		centered: true,
		wgpu_options: eframe::egui_wgpu::WgpuConfiguration::default(),
	};
	
	let (tx, rx) = mpsc::channel();
	let (txLexiTasks, rxUi) = mpsc::channel();
	let mut lexiTasks = LexiTasks::new(rx, txLexiTasks);
	let lists = lexiTasks.getLists();
	lexiTasks.load();
	let threadTasks = thread::Builder::new().name("tasks".to_string()).spawn(move ||{
		loop {
			lexiTasks.update();
			if lexiTasks.exit() {
				break;
			}
		}
	}).unwrap();
	let tx2 = tx.clone();
	eframe::run_native("LexiTasks", eOptions, Box::new(move |creationContext| Box::new(LexiUi::new(creationContext, tx2, rxUi, lists))));
	
	//termination
	tx.send(ThreadMessage::terminate).expect("is threadTasks dead?");
	threadTasks.join().unwrap();
}

pub enum ThreadMessage {
	terminate,
	error(String),
	newTask(u32),
	newList,
	createdTask((u32,u32)),
}