use super::*;
use std::fs::File;
use std::fs;
use std::io::Write;
use std::io::ErrorKind;

#[cfg(debug_assertions)]
use std::path::PathBuf;

impl LexiTasks {
pub fn save(&mut self) -> Result<(),&'static str>{ // can be better but works for now.
	let mut path = match dirs::data_dir() {
		Some(path) => path,
		None => {
			self.tx.send(ThreadMessage::error("couldn't get data_dir\n".into())).expect("did LexiUi die?");
			return Err("couldn't get data_dir");
		}
	};
	path.push(env!("CARGO_PKG_NAME"));
	path.push("lists");
	
	#[cfg(debug_assertions)]
	let mut path = PathBuf::new();
	#[cfg(debug_assertions)]
	path.push("listexampleOutput");
	
	let mut file = match File::create(path.clone()) {
		Ok(f) => f,
		Err(e) => match e.kind() {
			ErrorKind::NotFound => {
				match fs::create_dir(path.parent().unwrap()) {
					Ok(..) => return self.save(),
					Err(e) => match e.kind() {
						ErrorKind::PermissionDenied => {
							self.tx.send(ThreadMessage::error("permision denied when creating save dir\n".into())).expect("did LexiUi die?");
							return Err("permision denied when creating save dir");
						},
						_ => {
							self.tx.send(ThreadMessage::error("unregistered error ocurred when creating save dir\n".into())).expect("did LexiUi die?");
							return Err("unregistered error ocurred when creating save dir");
						}
					}
				}
			},
			ErrorKind::PermissionDenied => {
				self.tx.send(ThreadMessage::error("permision denied when creating save file\n".into())).expect("did LexiUi die?");
				return Err("permision denied when creating save file");
			},
			_ => {
				self.tx.send(ThreadMessage::error("unregistered error when creating save file\n".into())).expect("did LexiUi die?");
				return Err("unregistered error ocurred when creating save file");
			}
		}
	};
	
	let lists = self.lists.lock().unwrap();
	writeln!(file,"this file should not be manualy edited").unwrap();
	writeln!(file,"version:{}",Self::VERSION).unwrap();
	
	for list in lists.iter() {
		writeln!(file,"list{{").unwrap();
		writeln!(file,"\tname:[{}]",list.name).unwrap();
		for task in list.tasks.iter() {
			writeln!(file,"\ttask:{}",task).unwrap();
		}
		writeln!(file,"}}end").unwrap();
	}

	println!("data saved");
	Ok(())
}}