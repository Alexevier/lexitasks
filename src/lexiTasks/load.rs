use super::LexiTasks;
use crate::task::*;
use crate::list::*;
use crate::ThreadMessage;
use std::fs::File;
// use std::fs::OpenOptions;
use std::io::Read;
use std::io::ErrorKind;

/* NOTE
 * Sending message to UI should not fail and if it does is safe to panic.
*/

impl LexiTasks {
pub fn load(&mut self){ // can be better but works for now.
	let mut path = match dirs::data_dir() {
		Some(path) => path,
		None => {
			self.tx.send(ThreadMessage::error("couldn't get data_dir\n".into())).expect("did LexiUi die?");
			return ();
		}
	};
	path.push(env!("CARGO_PKG_NAME"));
	path.push("lists");
	
	#[cfg(debug_assertions)]
	let path = "listexample".to_owned();
	
	let mut file = match File::open(path.clone()) {
		Ok(f) => f,
		Err(e) => match e.kind() {
			ErrorKind::NotFound => {
				println!("\"{:?}\" doesn't exist or wasn't found.\nMay be the first run?",path);
				return ()
			},
			ErrorKind::PermissionDenied => {
				println!("user doesn't has permsions for \"{:?}\"?",path);
				self.tx.send((ThreadMessage::error(format!("user doesn't has permsions for \"{:?}\"?",path)))).unwrap();
				return ()
			},
			_ => {
				println!("unkown/unregistered error ocurred opening \"{:?}\"",path);
				self.tx.send((ThreadMessage::error(format!("unkown/unregistered error ocurred opening \"{:?}\"",path)))).unwrap();
				return ()
			}
		}
	};
	
	let mut content = String::new();
	match file.read_to_string(&mut content) {
		Ok(..) => {},
		Err(e) => {
			println!("error \"{}\" ocured while reading \"{:?}\"", e, path);
			self.tx.send((ThreadMessage::error(format!("error \"{}\" ocured while reading \"{:?}\"", e, path)))).unwrap();
			return ()
		}
	};
	drop(file);
	
	let mut listVersion: u8 = 0;
	
	let line = match content.lines().nth(1) {
		Some(l) => l,
		None => {
			println!("tasks file seems to be incomplete, version line not found.");
			self.tx.send(ThreadMessage::error(format!("tasks file seems to be incomplete, version line not found."))).unwrap();
			return ();
		}
	};
	
	let data: Vec<&str> = line.split(':').collect();
		if data.len() != 2 {
			println!("list version is incomplete");
			self.tx.send(ThreadMessage::error("list version is incomplete".to_owned())).unwrap();
			return();
		};
		if data[0] == "version" {
			listVersion = match data[1].parse() {
				Ok(v) => v,
				Err(..) => {println!("list version \"{}\" is invalid",data[1]); return();}
			};
		}
	
	
	//here goes updating the list in case it comes from older version.
	match listVersion {
		0 => {
			#[cfg(not(debug_assertions))]
			{
				self.tx.send(ThreadMessage::error(format!("tasksList format v0 is for development only.\nSupported versions are 1."))).unwrap();
				return ();
			}
		},
		1 => {},
		_=> {
			println!("tasksList format v{} is not supported.\nSupported versions are 0", listVersion);
			self.tx.send(ThreadMessage::error(format!("tasksList format v{} is not supported.\nSupported versions are 1.", listVersion))).unwrap();
		},
	}
	
	
	let mut lists = self.lists.lock().unwrap();
	let mut listN: usize = 0;
	let mut listOpen: bool = false;
	let mut listNamed: bool = false;
	let mut taskN: usize = 0;
	let mut brackOpen: bool = false;
	let mut chars = content.chars();
	let mut buff = String::new();
	'parsing: loop{
		//set the char
		let mut char = match chars.next() {
			Some(c) => c,
			None => break 'parsing
		};
		buff.push(char);
		
		//check if list opens and closes
		if buff.ends_with("list{") && !listOpen {
			listOpen = true;
			lists.push(List::new());
			buff.clear();
		}
		if buff.ends_with("}end") && listOpen {
			if !listNamed {
				lists[listN].name = format!("NULL{}",listN);
				self.tx.send(ThreadMessage::error(format!("a list doesn't have name, renamed to: {}",lists[listN].name))).expect("did LexiUi die?");
			}
			listOpen = false;
			listNamed = false;
			listN += 1;
			taskN = 0;
			buff.clear();
		}
		
		//get list name
		if buff.ends_with("name:") && !listNamed {
			for chari in chars.clone() {
				if chari == '[' {
					brackOpen = true;
					lists[listN].name.clear();
					continue;
				} else if chari == ']' {
					listNamed = true;
					brackOpen = false;
				}
				if !brackOpen {
					break;
				}
				lists[listN].name.push(chari);
			}
		}
		
		//get tasks
		if buff.ends_with("task:") {
			lists[listN].tasks.push(Task::default());
			let mut taskVar: u8 = 0; // 0 = title, 1 = notes, 2 = get priority buff, 3 = parse priority, 4 = get done buff, 5 = parse done, 6 _ finnish parsing task
			buff.clear();
			'parsingTask: loop{
				char = match chars.next() {
					Some(c) => c,
					None => break 'parsing
				};
				if char == '[' {
					brackOpen = true;
					continue;
				}
				if char == ']' {
					brackOpen = false;
					taskVar += 1;
				}
				match taskVar {
					0 => {
						if brackOpen { lists[listN].tasks[taskN].title.push(char) };
					},
					1 => {
						if brackOpen { lists[listN].tasks[taskN].notes.push(char) };
					},
					2 => {
						if brackOpen { buff.push(char) };
					},
					3 => {
						lists[listN].tasks[taskN].priority = match buff.parse() {
							Ok(v) => v,
							Err(e) => {println!("a task contains: {:?}", e); Priority::none}
						};
						buff.clear();
						taskVar += 1;
					},
					4 => {
						buff.push(char)
					},
					5 => {
						lists[listN].tasks[taskN].done = match buff.parse() {
							Ok(v) => v,
							Err(e) => {println!("a task contains: {:?}", e); false}
						};
						taskVar += 1;
					},
					6 => {
						taskN += 1;
						break 'parsingTask;
					},
					_ => println!("taskVar in LexiTasks.load() was invalid somehow")
				}
			}
		}
	}
	println!("data loaded");
}
}