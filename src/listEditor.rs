use crate::config;
use crate::task::*;
use crate::List;
use std::sync::{Arc, Mutex};

pub struct ListEditor {
	done: bool,
	action: Action,
	listId: u32,
	nameBuff: String,
	lists: Arc<Mutex<Vec<List>>>,
}

impl ListEditor {
pub fn new(lists: Arc<Mutex<Vec<List>>>) -> Self {
	Self {
		done: true,
		action: Action::none,
		listId: 0,
		nameBuff: String::new(),
		lists,
	}
}

fn clean(&mut self) {
	self.done = true;
	self.action = Action::none;
	self.listId = 0;
	self.nameBuff.clear();
}

pub fn done(&self) -> bool {
	self.done
}

pub fn set(&mut self, list: &List, action: Action) {
	self.clean();
	self.done = false;
	self.action = action;
	self.listId = list.id();
	self.nameBuff = list.name.clone();
}

pub fn setId(&mut self, listId: u32, action: Action) {
	self.clean();
	self.action = action;
	self.listId = listId;
}

pub fn update(&mut self) {
match self.action {
	Action::none => {},
	Action::delete => { self.doDelete(); self.clean(); },
	Action::new => {},
	Action::edit => { if self.done { self.write(); self.clean(); }},
	Action::moveUp |
	Action::moveDown |
	Action::moveTop |
	Action::moveBottom => { self.doMove(); self.clean(); },
}}

fn doDelete(&mut self) {
	let mut i = 0;
	let mut lists = self.lists.lock().unwrap();
	for list in lists.iter_mut() {
		if list.id() == self.listId {
			lists.remove(i);
			return;
		}
		i += 1;
	}
}

fn write(&mut self) {
	for list in self.lists.lock().unwrap().iter_mut() {
		if list.id() == self.listId {
			//remove invalid chars before write
			let mut j = 0;
			for char in self.nameBuff.clone().chars() {
				if char == '[' || char == ']' || char == '{' || char == '}'  {
					self.nameBuff.remove(j);
					j-=1;
				}
				j+=1;
			}
			list.name = self.nameBuff.clone();
			return;
		}
	}
}

fn doMove(&mut self) {
	let mut i = 0;
	let mut lists = self.lists.lock().unwrap();
	for list in lists.iter_mut() {
		if list.id() == self.listId {
			let mut n;
			if self.action == Action::moveUp && i == 0 { return; }
			if self.action == Action::moveDown && i >= lists.len()-1 { return; }
			if self.action == Action::moveUp { n = i - 1 }
			else { n = i + 1 }
			if self.action == Action::moveTop { n = 0; }
			if self.action == Action::moveBottom { n = lists.len()-1 }
			let list = lists.remove(i);
			lists.insert(n,list);
			return;
		}
		i += 1;
	}
}
}

use egui::RichText;
use egui::FontId;
use egui::TextEdit;
use egui::Color32;

impl ListEditor {
	pub fn ui(&mut self, ctx: &egui::Context, textures: &[egui::TextureHandle]) {
		egui::Window::new(RichText::new("ListEditor")).collapsible(false).resizable(true).show(ctx, |ui| {
			ui.horizontal(|ui|{
				if config().showIcons {
					let saveButton = egui::widgets::ImageButton::new(&textures[5], egui::Vec2::new(18.,18.));
					let deleteButton = egui::widgets::ImageButton::new(&textures[4], egui::Vec2::new(18.,18.));
					if ui.add(saveButton).clicked() { self.done = true; }
					if ui.add(deleteButton).clicked() { self.setId(self.listId, Action::delete); }
				} else {
					if ui.button(RichText::new("Save").size(18.)).clicked() { self.done = true; }
					if ui.button(RichText::new("Delete").size(18.)).clicked() { self.setId(self.listId, Action::delete); }
				}
				ui.with_layout(egui::Layout::top_down(egui::Align::Min),|ui| {
				if config().showIcons {
					let upButton = egui::widgets::ImageButton::new(&textures[6], egui::Vec2::new(12.,7.));
					let downButton = egui::widgets::ImageButton::new(&textures[7], egui::Vec2::new(12.,7.));
					if ui.add(upButton).clicked() {
						if ui.input().modifiers.shift { self.setId(self.listId, Action::moveTop); }
						else { self.setId(self.listId, Action::moveUp); }
					};
					if ui.add(downButton).clicked() {
						if ui.input().modifiers.shift { self.setId(self.listId, Action::moveBottom); }
						else { self.setId(self.listId, Action::moveDown); }
					};
				} else {
					if ui.small_button(RichText::new("Up").color(Color32::WHITE).size(13.)).clicked() {
						if ui.input().modifiers.shift { self.setId(self.listId, Action::moveTop); }
						else { self.setId(self.listId, Action::moveUp); }
					};
					if ui.small_button(RichText::new("Down").color(Color32::WHITE).size(13.)).clicked() {
						if ui.input().modifiers.shift { self.setId(self.listId, Action::moveBottom); }
						else { self.setId(self.listId, Action::moveDown); }
					};
				}
				});
				if config().showIcons {
				let cancelButton = egui::widgets::ImageButton::new(&textures[3], egui::Vec2::new(18.,18.));
				if ui.add(cancelButton).clicked() {
					if self.action == Action::new {
						self.doDelete();
						self.clean();
					} else {
						self.clean();
					}
					return;
				}
				} else {
					if ui.button(RichText::new("Cancel").size(18.)).clicked() {
						if self.action == Action::new {
							self.doDelete();
							self.clean();
						} else {
							self.clean();
						}
						return;
					}
				}
			});
			let nameEdit = TextEdit::multiline(&mut self.nameBuff).font(FontId::monospace(20.));
			ui.add(nameEdit);
		});
	}
}