use super::*;
use egui::ScrollArea;
use egui::RichText;
use egui::Color32;

impl LexiUi{
pub fn configWindow(&mut self, ctx: &egui::Context){
	egui::Window::new(RichText::new("Config").color(Color32::WHITE).size(22.)).collapsible(false).resizable(true).open(&mut self.showConfig).show(ctx, |ui| {
		ScrollArea::vertical().always_show_scroll(config().showScrollAlways).auto_shrink([false; 2]).show(ui, |ui| {
			if config().showDangerousConfigs {
				ui.label(RichText::new("SHOWING DANGEROUS CONFIGS").color(Color32::RED).size(20.));
				ui.label(RichText::new("Some configs require a restart!").color(Color32::GRAY).size(16.));
				ui.separator();
			}
			ui.checkbox(&mut config().showIcons, RichText::new("Use icons instead of text").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().showDoneSamePanel, RichText::new("Show done tasks in the same panel").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().showDonePanel, RichText::new("Show done panel").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().showScrollAlways, RichText::new("Always show scrollbars").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().showDangerousConfigs, RichText::new("Shows dangerous configs").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().sidePanelLeft, RichText::new("SidePanel on the left").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().hideEmptyNotes, RichText::new("Hide notes if empty").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().createTasksTop, RichText::new("Create new tasks at the top of the list").color(Color32::WHITE).size(20.));
			ui.checkbox(&mut config().hardwareAcceleration, RichText::new("HardwareAcceleration").color(Color32::WHITE).size(20.));
			if config().showDangerousConfigs {
			ui.menu_button(RichText::new(format!("Shader Version: {{}}")).color(Color32::RED).size(20.), |ui| {
				if ui.button(RichText::new("Gl120").color(Color32::WHITE).size(18.)).clicked() {
					config().shaderVersion = eframe::egui_glow::ShaderVersion::Gl120;
					ui.close_menu();
				}
				if ui.button(RichText::new("Gl140").color(Color32::WHITE).size(18.)).clicked() {
					config().shaderVersion = eframe::egui_glow::ShaderVersion::Gl140;
					ui.close_menu();
				}
				if ui.button(RichText::new("Es100").color(Color32::WHITE).size(18.)).clicked() {
					config().shaderVersion = eframe::egui_glow::ShaderVersion::Es100;
					ui.close_menu();
				}
				if ui.button(RichText::new("Es300").color(Color32::WHITE).size(18.)).clicked() {
					config().shaderVersion = eframe::egui_glow::ShaderVersion::Es300;
					ui.close_menu();
				}
			});
			ui.menu_button(RichText::new(format!("Renderer: {}",config().renderer)).color(Color32::RED).size(20.), |ui| {
				if ui.button(RichText::new("Glow (default)").color(Color32::WHITE).size(18.)).clicked() {
					config().renderer = eframe::Renderer::Glow;
					ui.close_menu();
				}
				if ui.button(RichText::new("Wgpu").color(Color32::WHITE).size(18.)).clicked() {
					config().renderer = eframe::Renderer::Wgpu;
					ui.close_menu();
				}
			});
			}
			ui.menu_button(RichText::new(format!("Autosave every {} seconds", config().autosaveTime )).color(Color32::WHITE).size(20.), |ui| {
				if ui.button(RichText::new("60").color(Color32::WHITE).size(18.)).clicked() {
					config().autosaveTime = 60;
					ui.close_menu();
				}
				if ui.button(RichText::new("45 (default)").color(Color32::WHITE).size(18.)).clicked() {
					config().autosaveTime = 45;
					ui.close_menu();
				}
				if ui.button(RichText::new("30").color(Color32::WHITE).size(18.)).clicked() {
					config().autosaveTime = 30;
					ui.close_menu();
				}
				if ui.button(RichText::new("15").color(Color32::WHITE).size(18.)).clicked() {
					config().autosaveTime = 15;
					ui.close_menu();
				}
				if ui.button(RichText::new("5").color(Color32::WHITE).size(18.)).clicked() {
					config().autosaveTime = 5;
					ui.close_menu();
				}
			});
			ui.menu_button(RichText::new(format!("Update every {} milliseconds", config().updateTime )).color(Color32::WHITE).size(20.), |ui| {
				if ui.button(RichText::new("25").color(Color32::WHITE).size(18.)).clicked() {
					config().updateTime = 25;
					ui.close_menu();
				}
				if ui.button(RichText::new("50 (default)").color(Color32::WHITE).size(18.)).clicked() {
					config().updateTime = 50;
					ui.close_menu();
				}
				if ui.button(RichText::new("100").color(Color32::WHITE).size(18.)).clicked() {
					config().updateTime = 100;
					ui.close_menu();
				}
				if ui.button(RichText::new("200").color(Color32::WHITE).size(18.)).clicked() {
					config().updateTime = 200;
					ui.close_menu();
				}
				if ui.button(RichText::new("500").color(Color32::WHITE).size(18.)).clicked() {
					config().updateTime = 500;
					ui.close_menu();
				}
				if ui.button(RichText::new("999").color(Color32::WHITE).size(18.)).clicked() {
					config().updateTime = 999;
					ui.close_menu();
				}
			});
		});
	});
}}