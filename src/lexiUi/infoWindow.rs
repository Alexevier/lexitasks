use super::*;
use egui::RichText;
use egui::Color32;

impl LexiUi {
pub fn infoWindow(&mut self, ctx: &egui::Context) {
	egui::Window::new(RichText::new("Info").color(Color32::WHITE).size(22.)).collapsible(false).resizable(true).open(&mut self.showInfo).show(ctx, |ui| {
		ui.label(RichText::new(format!("LexiTask v{}",env!("CARGO_PKG_VERSION"))).color(Color32::WHITE).size(25.));
		ui.separator();
		ui.label(RichText::new("Made by Alexevier").color(Color32::WHITE).size(20.));
		ui.horizontal(|ui|{
			ui.label(RichText::new("Under the").color(Color32::WHITE).size(20.));
			ui.hyperlink_to(RichText::new("GLPv2 license").size(20.), "https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html");
		});
		ui.separator();
		ui.horizontal(|ui|{
			ui.label(RichText::new("GUI made with").color(Color32::WHITE).size(20.));
			ui.hyperlink_to(RichText::new("eframe").size(20.), "https://crates.io/crates/eframe");
		});
		ui.horizontal(|ui|{
			ui.label(RichText::new("Finding directories with").color(Color32::WHITE).size(20.));
			ui.hyperlink_to(RichText::new("dirs").size(20.), "https://crates.io/crates/dirs");
		});
		ui.separator();
		ui.horizontal(|ui|{
			ui.label(RichText::new("Project Repo on").color(Color32::WHITE).size(20.));
			ui.hyperlink_to(RichText::new("gitlab").size(20.), env!("CARGO_PKG_REPOSITORY"));
		});
	});
}}