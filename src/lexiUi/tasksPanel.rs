use super::*;
use crate::*;
use crate::task::Action;
use egui::CentralPanel;
use egui::ScrollArea;

impl LexiUi {
pub fn tasksPanel (&mut self, ctx: &egui::Context){
CentralPanel::default().show(ctx, |ui| {
ScrollArea::vertical().always_show_scroll(config().showScrollAlways).auto_shrink([false; 2]).show(ui, |ui|{
	for list in self.lists.lock().unwrap().iter_mut() {
		let listId = list.id();
		if list.id() == self.activeListId{ 
			for task in list.tasks.iter_mut() {
				if !task.done || config().showDoneSamePanel {
					match task.ui(ui, &self.textures) {
						Action::none => {},
						Action::new => {},
						Action::edit => {
							self.taskEditor.set(listId,task,Action::edit);
							self.showTaskEdit = true;
						}
						Action::delete => {
							self.taskEditor.set(listId,task,Action::delete);
						},
						Action::moveUp => {
							self.taskEditor.setIds(listId,task.id,Action::moveUp);
						},
						Action::moveDown => {
							self.taskEditor.setIds(listId,task.id,Action::moveDown)
						},
						Action::moveTop => {
							self.taskEditor.setIds(listId,task.id,Action::moveTop)
						},
						Action::moveBottom => {
							self.taskEditor.setIds(listId,task.id,Action::moveBottom)
						},
					};
				}
			}
		}
	}
})});
}
}