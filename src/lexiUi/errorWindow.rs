use super::*;
use egui::ScrollArea;
use egui::RichText;
use egui::Color32;

impl LexiUi{
pub fn errorWindow(&mut self, ctx: &egui::Context){
	egui::Window::new(RichText::new("ERROR").color(Color32::RED)).collapsible(false).resizable(true).open(&mut self.showError).show(ctx, |ui| {
		ScrollArea::vertical().always_show_scroll(config().showScrollAlways).auto_shrink([false; 2]).show(ui, |ui| {
			ui.label(RichText::new(&self.errorMsg).color(Color32::WHITE).size(20.));
		});
	});
}
}