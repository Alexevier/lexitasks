use super::*;
use egui::SidePanel;
use egui::ScrollArea;

impl LexiUi {
pub fn donePanel (&mut self, ctx: &egui::Context){
SidePanel::right("donePanel").default_width(280.).resizable(true).show(ctx, |ui| {
ScrollArea::vertical().always_show_scroll(config().showScrollAlways).auto_shrink([false; 2]).show(ui, |ui|{
	for list in self.lists.lock().unwrap().iter_mut() {
		let listId = list.id();
		if list.id() == self.activeListId{ 
			for task in list.tasks.iter_mut() {
				if task.done {
					match task.ui(ui, &self.textures) {
						Action::none => {},
						Action::new => {},
						Action::edit => {
							self.taskEditor.set(listId,&task,Action::edit);
							self.showTaskEdit = true;
						}
						Action::delete => {
							self.taskEditor.set(listId,&task,Action::delete);
						},
						Action::moveUp => {
							self.taskEditor.setIds(listId,task.id,Action::moveUp)
						},
						Action::moveDown => {
							self.taskEditor.setIds(listId,task.id,Action::moveDown)
						},
						Action::moveTop => {
							self.taskEditor.setIds(listId,task.id,Action::moveTop)
						},
						Action::moveBottom => {
							self.taskEditor.setIds(listId,task.id,Action::moveBottom)
						},
					};
				}
			}
		}
	}
});
});
}
}