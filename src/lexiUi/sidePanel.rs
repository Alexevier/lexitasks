use super::*;
use egui::SidePanel;
use egui::Layout;
use egui::Align;
use egui::RichText;
use egui::Color32;

impl LexiUi {
pub fn sidePanel(&mut self, ctx: &egui::Context){
	let panel = if config().sidePanelLeft {
		SidePanel::left("sidePanel")
	} else {
		SidePanel::right("sidePanel")
	};
	
	panel.resizable(false).max_width(155.).show(ctx, |ui| {
		ui.label(RichText::new("Lists").color(Color32::WHITE).size(20.));
		ui.separator();
		for list in self.lists.lock().unwrap().iter() {
			ui.horizontal_wrapped(|ui|{
				if ui.button(RichText::new(&list.name).color(Color32::WHITE).size(18.)).clicked() {
					self.activeListId = list.id();
				};
				if config().showIcons {
					let editButton = egui::widgets::ImageButton::new(&self.textures[1], egui::Vec2::new(18.,18.));
					if ui.add(editButton).clicked() {
						self.listEditor.set(&list, Action::edit);
					};
				} else {
					if ui.small_button(RichText::new("E").color(Color32::WHITE).size(18.)).clicked() {
						self.listEditor.set(&list, Action::edit);
					}
				}
			});
		}
		
		
		ui.with_layout(Layout::bottom_up(Align::Min),|ui| {
			if config().showIcons{
				let configButton = egui::widgets::ImageButton::new(&self.textures[8], egui::Vec2::new(20.,20.));
				let newListButton = egui::widgets::ImageButton::new(&self.textures[10], egui::Vec2::new(20.,20.));
				let newTaskButton = egui::widgets::ImageButton::new(&self.textures[9], egui::Vec2::new(20.,20.));
				let infoButton = egui::widgets::ImageButton::new(&self.textures[11], egui::Vec2::new(20.,20.));
				ui.horizontal(|ui|{
					if ui.add(configButton).clicked() {
						self.showConfig = true;
					}
					if ui.add(newListButton).clicked() {
						self.tx.send(ThreadMessage::newList).expect("did LexiTasks die?");
					}
					if ui.add(newTaskButton).clicked() {
						self.tx.send(ThreadMessage::newTask(self.activeListId)).expect("did LexiTasks die?");
					}
					if ui.add(infoButton).clicked() {
						self.showInfo = true;
					}
				});
			} else {
				if ui.button(RichText::new("Config").color(Color32::WHITE).size(18.)).clicked() {
					self.showConfig = true;
				}
				if ui.button(RichText::new("New List").color(Color32::WHITE).size(18.)).clicked() {
					self.tx.send(ThreadMessage::newList).expect("did LexiTasks die?");
				};
				if ui.button(RichText::new("New Task").color(Color32::WHITE).size(18.)).clicked() {
					self.tx.send(ThreadMessage::newTask(self.activeListId)).expect("did LexiTasks die?");
				};
				if ui.button(RichText::new("Info").color(Color32::WHITE).size(18.)).clicked() {
					self.showInfo = true;
				}
			}
			ui.separator();
		});
	});
}
}