mod donePanel;
mod tasksPanel;
mod sidePanel;
mod errorWindow;
mod configWindow;
mod infoWindow;

use std::sync::{Mutex, Arc};
use std::sync::mpsc::TryRecvError;
use std::sync::mpsc;
use crate::ThreadMessage;
use crate::config;
use crate::task::*;
use crate::List;
use crate::TaskEditor;
use crate::ListEditor;

pub struct LexiUi {
	errorMsg: String,
	showError: bool,
	showConfig: bool,
	showTaskEdit: bool,
	showInfo: bool,
	taskEditor: TaskEditor,
	listEditor: ListEditor,
	tx: mpsc::Sender<ThreadMessage>,
	rx: mpsc::Receiver<ThreadMessage>,
	lists: Arc<Mutex<Vec<List>>>,
	activeListId: u32,
	textures: [egui::TextureHandle; 12],
}

impl LexiUi {
pub fn new (cc: &eframe::CreationContext<'_>, tx: mpsc::Sender<ThreadMessage>, rx: mpsc::Receiver<ThreadMessage>, lists: Arc<Mutex<Vec<List>>>) -> Self {
	Self{
		errorMsg: String::new(),
		showError: false,
		showConfig: false,
		showTaskEdit: false,
		showInfo: false,
		taskEditor: TaskEditor::new(lists.clone()),
		listEditor: ListEditor::new(lists.clone()),
		tx,
		rx,
		lists,
		activeListId: 1,
		textures: [
			loadTextureBytes(&cc.egui_ctx, "icon", include_bytes!("textures/icon.png")), //0
			loadTextureBytes(&cc.egui_ctx, "edit", include_bytes!("textures/edit.png")), //1
			loadTextureBytes(&cc.egui_ctx, "done", include_bytes!("textures/done.png")), //2
			loadTextureBytes(&cc.egui_ctx, "x", include_bytes!("textures/x.png")), //3
			loadTextureBytes(&cc.egui_ctx, "trash", include_bytes!("textures/trash.png")), //4
			loadTextureBytes(&cc.egui_ctx, "floppy", include_bytes!("textures/floppy.png")), //5
			loadTextureBytes(&cc.egui_ctx, "arrowUp", include_bytes!("textures/arrowUp.png")), //6
			loadTextureBytes(&cc.egui_ctx, "arrowDown", include_bytes!("textures/arrowDown.png")), //7
			loadTextureBytes(&cc.egui_ctx, "config", include_bytes!("textures/config.png")), //8
			loadTextureBytes(&cc.egui_ctx, "newTask", include_bytes!("textures/newTask.png")), //9
			loadTextureBytes(&cc.egui_ctx, "newList", include_bytes!("textures/newList.png")), //10
			loadTextureBytes(&cc.egui_ctx, "info", include_bytes!("textures/info.png")), //11
		]
	}
}
}

impl eframe::App for LexiUi {
fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
	self.sidePanel(ctx);
	match self.rx.try_recv() {
		Ok(msg) => match msg {
			ThreadMessage::error(m) => {
				self.showError = true;
				self.errorMsg += &m;
			},
			ThreadMessage::createdTask(id) => {self.taskEditor.setIds(id.0,id.1,Action::new); self.showTaskEdit = true;}
			_ => println!("LexiUi received unregistered threadMessage")
		},
		Err(e) => match e {
			TryRecvError::Empty => {},
			TryRecvError::Disconnected => {
				panic!("did LexiTasks thread die?")
			}
		}
	}
	
	self.taskEditor.update();
	self.listEditor.update();
	if self.showTaskEdit { self.taskEditor.ui(ctx, &self.textures); }
	if self.taskEditor.done() { self.showTaskEdit = false; }
	if !self.listEditor.done() { self.listEditor.ui(ctx, &self.textures); }
		
	if self.showConfig {
		self.configWindow(ctx);
	}
	if self.showError {
		self.errorWindow(ctx);
	} else {
		self.errorMsg.clear();
	}
	if self.showInfo {
		self.infoWindow(ctx);
	}
	if config().showDonePanel && !config().showDoneSamePanel {
		self.donePanel(ctx);
	}
	self.tasksPanel(ctx);
	ctx.request_repaint_after(std::time::Duration::from_millis((config().updateTime + 10).into()));
}
}

//this will get moved to lexlib as a feature for egui
fn loadTextureBytes(ctx: &egui::Context, name: &str, img: &[u8]) -> egui::TextureHandle {
	let texture: egui::TextureHandle = 
	ctx.load_texture(
		name,
		egui_extras::image::load_image_bytes(img).unwrap(),
		egui::TextureOptions::default()
	);
	texture
}