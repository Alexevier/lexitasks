use crate::config;
use crate::task::*;
use crate::List;
use std::sync::{Arc, Mutex};

pub struct TaskEditor {
	done: bool,
	action: Action,
	listId: u32,
	taskId: u32,
	titleBuff: String,
	notesBuff: String,
	priorityBuff: Priority,
	lists: Arc<Mutex<Vec<List>>>,
}

impl TaskEditor {
pub fn new(lists: Arc<Mutex<Vec<List>>>) -> Self {
	Self {
		done: true,
		action: Action::none,
		listId: 0,
		taskId: 0,
		titleBuff: String::new(),
		notesBuff: String::new(),
		priorityBuff: Priority::normal,
		lists,
	}
}

fn clean(&mut self) {
	self.done = true;
	self.action = Action::none;
	self.listId = 0;
	self.taskId = 0;
	self.titleBuff.clear();
	self.notesBuff.clear();
	self.priorityBuff = Priority::normal;
}

pub fn done(&self) -> bool {
	self.done
}

pub fn set(&mut self, listId: u32, task: &Task, action: Action) {
	self.clean();
	self.done = false;
	self.listId = listId;
	self.taskId = task.id;
	self.action = action;
	if self.action == Action::edit {
		self.titleBuff = task.title.clone();
		self.notesBuff = task.notes.clone();
		self.priorityBuff = task.priority;
	}
}

pub fn setIds(&mut self, listId: u32, taskId: u32, action: Action) {
	self.clean();
	self.done = false;
	self.listId = listId;
	self.taskId = taskId;
	self.action = action;
}

pub fn update(&mut self) {
match self.action {
	Action::none => {},
	Action::delete => { self.doDelete(); self.clean(); },
	Action::new => { if self.done { self.write(); self.clean(); }},
	Action::edit => { if self.done { self.write(); self.clean(); }},
	Action::moveUp |
	Action::moveDown |
	Action::moveTop |
	Action::moveBottom => { self.doMove(); self.clean(); },
}
}

fn doDelete(&mut self) {
	for list in self.lists.lock().unwrap().iter_mut() {
	if list.id() == self.listId {
		let mut i = 0;
		for task in list.tasks.iter_mut() {
		if task.id == self.taskId {
			list.tasks.remove(i);
			return ();
		}i += 1;}
	}}
}

fn write(&mut self) {
	for list in self.lists.lock().unwrap().iter_mut() {
	if list.id() == self.listId {
		let mut i = 0;
		for task in list.tasks.iter_mut() {
		if task.id == self.taskId {
			//remove invalid chars before write
			let mut j = 0;
			for char in self.titleBuff.clone().chars() {
				if char == '[' || char == ']' || char == '{' || char == '}'  {
					self.titleBuff.remove(j);
					j-=1;
				}
				j+=1;
			}
			let mut j = 0;
			for char in self.notesBuff.clone().chars() {
				if char == '[' || char == ']' || char == '{' || char == '}' {
					self.notesBuff.remove(j);
					j-=1;
				}
				j+=1;
			}
			//write
			list.tasks[i].title = self.titleBuff.clone();
			list.tasks[i].notes = self.notesBuff.clone();
			list.tasks[i].priority = self.priorityBuff;
			return ();
		}i += 1;}
	}}
}

fn doMove(&mut self) {
	for list in self.lists.lock().unwrap().iter_mut() {
	if list.id() == self.listId {
		let mut i = 0;
		for task in list.tasks.iter_mut() {
		if task.id == self.taskId {
			let mut n;
			if self.action == Action::moveUp && i == 0 { return (); }
			if self.action == Action::moveDown && i >= list.tasks.len()-1 { return (); }
			if self.action == Action::moveUp { n = i - 1 }
			else { n = i + 1 }
			if self.action == Action::moveTop { n = 0; }
			if self.action == Action::moveBottom { n = list.tasks.len()-1 }
			let task = list.tasks.remove(i);
			list.tasks.insert(n,task);
			return ();
		}i += 1;}
	}}
}
}

use egui::RichText;
use egui::FontId;
use egui::TextEdit;

impl TaskEditor {
pub fn ui(&mut self, ctx: &egui::Context, textures: &[egui::TextureHandle]){
	egui::Window::new(RichText::new("TaskEditor")).collapsible(false).resizable(true).show(ctx, |ui| {
		ui.horizontal(|ui|{
			if config().showIcons {
				let saveButton = egui::widgets::ImageButton::new(&textures[5], egui::Vec2::new(18.,18.));
				if ui.add(saveButton).clicked() {
					self.done = true;
				}
			} else {
				if ui.button(RichText::new("Save").size(18.)).clicked() {
					self.done = true;
				}
			}
			ui.menu_button(RichText::new("Priority").size(18.), |ui| {
				if ui.button(RichText::new("Urgent").size(16.)).clicked() { ui.close_menu(); self.priorityBuff = Priority::urgent }
				if ui.button(RichText::new("High").size(16.)).clicked() { ui.close_menu(); self.priorityBuff = Priority::high }
				if ui.button(RichText::new("Normal").size(16.)).clicked() { ui.close_menu(); self.priorityBuff = Priority::normal }
				if ui.button(RichText::new("Low").size(16.)).clicked() { ui.close_menu(); self.priorityBuff = Priority::low }
				if ui.button(RichText::new("None").size(16.)).clicked() { ui.close_menu(); self.priorityBuff = Priority::none }
			});
			if config().showIcons {
				let cancelButton = egui::widgets::ImageButton::new(&textures[3], egui::Vec2::new(18.,18.));
				if ui.add(cancelButton).clicked() {
					if self.action == Action::new {
						self.doDelete();
						self.clean();
					} else {
						self.clean();
					}
					return;
				}
			} else {
				if ui.button(RichText::new("Cancel").size(18.)).clicked() {
					if self.action == Action::new {
						self.doDelete();
						self.clean();
					} else {
						self.clean();
					}
					return;
				}
			}

		});
		let titleEdit = TextEdit::singleline(&mut self.titleBuff).font(FontId::monospace(30.));
		let notesEdit = TextEdit::multiline(&mut self.notesBuff).font(FontId::monospace(20.));
		ui.add(titleEdit);
		ui.add(notesEdit);
	});
}
}