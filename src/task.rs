use crate::config;

#[derive(Debug, PartialEq, Clone)]
pub struct Task {
	pub id: u32,
	pub title: String,
	pub notes: String,
	pub done: bool,
	pub priority: Priority
}

impl Task {
// this might get removed since is never used
pub fn _new(title: &str, notes: &str, priority: Priority) -> Self {
	Self {
		id: 0,
		title: title.to_owned(),
		notes: notes.to_owned(),
		priority,
		done: false,
	}
}
}

use egui::Frame;
use egui::Style;
use egui::Color32;
use egui::RichText;

impl Task {
pub fn ui(&mut self, ui: &mut egui::Ui, textures: &[egui::TextureHandle]) -> Action{
	let style = Style::default();
	let mut action = Action::none;
	let color = match self.priority {
		Priority::urgent => config().colorUrgent,
		Priority::high => config().colorHigh,
		Priority::normal => config().colorNormal,
		Priority::low => config().colorLow,
		Priority::none => config().colorNone,
	};
	Frame::group(&style).fill(color.into()).show(ui, |ui| {
		ui.vertical_centered(|ui|{
			ui.label(RichText::new("").size(1.));
		});
		ui.horizontal_wrapped(|ui|{
			ui.with_layout(egui::Layout::top_down(egui::Align::Min),|ui| {
				if config().showIcons {
					let upButton = egui::widgets::ImageButton::new(&textures[6], egui::Vec2::new(13.,8.));
					let downButton = egui::widgets::ImageButton::new(&textures[7], egui::Vec2::new(13.,8.));
					if ui.add(upButton).clicked() {
						if ui.input().modifiers.shift { action = Action::moveTop; }
						else { action = Action::moveUp; }
					};
					if ui.add(downButton).clicked() {
						if ui.input().modifiers.shift { action = Action::moveBottom; }
						else { action = Action::moveDown; }
					};
				} else {
					if ui.small_button(RichText::new("Up").color(Color32::WHITE).size(13.)).clicked() {
						if ui.input().modifiers.shift { action = Action::moveTop; }
						else { action = Action::moveUp; }
					};
					if ui.small_button(RichText::new("Down").color(Color32::WHITE).size(13.)).clicked() {
						if ui.input().modifiers.shift { action = Action::moveBottom; }
						else { action = Action::moveDown; }
					};
				}
			});
			ui.heading(RichText::new(self.title.clone()).color(Color32::WHITE).size(30.));
			if config().showIcons {
				let editButton = egui::widgets::ImageButton::new(&textures[1], egui::Vec2::new(20.,20.));
				let doneButton = egui::widgets::ImageButton::new(&textures[2], egui::Vec2::new(20.,20.));
				let unDoneButton = egui::widgets::ImageButton::new(&textures[3], egui::Vec2::new(20.,20.));
				let deleteButton = egui::widgets::ImageButton::new(&textures[4], egui::Vec2::new(20.,20.));
				if ui.add(editButton).clicked() {
					action = Action::edit;
				};
				if ui.add(deleteButton).clicked() {
					action = Action::delete;
				};
				if self.done {
					if ui.add(unDoneButton).clicked() {
						self.done = false;
					};
				} else {
					if ui.add(doneButton).clicked() {
						self.done = true;
					};
				}
			} else {
				if ui.button(RichText::new("edit").color(Color32::WHITE).size(18.)).clicked() {
					action = Action::edit;
				};
				if ui.button(RichText::new("delete").color(Color32::WHITE).size(18.)).clicked() {
					action = Action::delete;
				};
				if self.done {
					if ui.button(RichText::new("unDone").color(Color32::WHITE).size(18.)).clicked() {
						self.done = false;
					};
				} else {
					if ui.button(RichText::new("done").color(Color32::WHITE).size(18.)).clicked() {
						self.done = true;
					};
				}
			}
		});
		if !self.notes.is_empty() || !config().hideEmptyNotes {
			ui.separator();
			ui.label(RichText::new(self.notes.clone()).color(Color32::WHITE).size(20.));
		}
	});
	action
}}

impl std::default::Default for Task {
fn default() -> Self {
	static mut TAKENIDS: u32 = 0;
	unsafe { TAKENIDS += 1 };
	Self{
		id: unsafe { TAKENIDS },
		title: String::from(""),
		notes: String::from(""),
		done: false,
		priority: Priority::normal
	}
}
}

use core::fmt;

impl fmt::Display for Task{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!{f, "[{}][{}][{}][{}]", self.title, self.notes, self.priority, self.done}
	}
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Priority {
	urgent,
	high,
	normal,
	low,
	none
}

impl fmt::Display for Priority{
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!{f, "{:?}", self}
	}
}

impl core::str::FromStr for Priority{
	type Err = TaskParseError;
	fn from_str(s: &str) -> Result<Self, Self::Err> {
		match s {
			"urgent" => Ok(Self::urgent),
			"high" => Ok(Self::high),
			"normal" => Ok(Self::normal),
			"low" => Ok(Self::low),
			"none" => Ok(Self::none),
			_=> Err(TaskParseError::invalidPriority)
		}
	}
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum TaskParseError{
	invalidPriority,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub enum Action {
	delete,
	new,
	edit,
	none,
	moveUp,
	moveDown,
	moveTop,
	moveBottom,
}